from time import time

import tensorflow as tf
from tensorflow import keras

import numpy as np

# Criação dos valores usados para treinar o modelo

#[ano, mes, campanhas de marketing]
train_data = np.array([
    [2018, 1, 1],[2018, 2, 1], [2018, 3, 1], [2018, 4, 1], [2018, 5, 2], [2018, 6, 1],
    [2018, 7, 2], [2018, 8, 2], [2018, 9, 1], [2018, 10, 3], [2018, 11, 6], [2018, 12, 5],
    [2019, 1, 4],[2019, 2, 2],[2019, 3, 2],[2019, 4, 1],[2019, 5, 3],[2019, 6, 1],
    [2019, 7, 1],[2019, 8, 1],[2019, 9, 2],[2019, 10, 4],[2019, 11, 7],[2019, 12, 6]
])

# [ itens vendidos no mês ]
train_labels = np.array(
    [10,3,2,5,8,5, 7,9,5,12,30,20, 15,5,3,3,11,7, 6,9,9,17,47,30]
)

print(train_data[0])
print(train_labels[0])

# embaralha os dados para evitar bias
order = np.argsort(np.random.random(train_labels.shape))
train_data = train_data[order]
train_labels = train_labels[order]

print(train_data[0])
print(train_labels[0])

# Normaliza os valores de uma forma que todos eles fiquem com a mesma representatividade
mean = train_data.mean(axis=0)
std = train_data.std(axis=0)
train_data = (train_data - mean) / std

print(train_data[0])
print(train_labels[0])

#Método de criação do modelo
def build_model():
  model = keras.Sequential([
    keras.layers.Dense(64, activation=tf.nn.relu,
                       input_shape=(train_data.shape[1],)),
    keras.layers.Dense(64, activation=tf.nn.relu),
    keras.layers.Dense(1)
  ])

  optimizer = tf.train.RMSPropOptimizer(0.01)

  model.compile(loss='mse',
                optimizer=optimizer,
                metrics=['mae'])
  return model

model = build_model()
#model.summary()

# Otimizador
early_stop = keras.callbacks.EarlyStopping(monitor='val_loss', patience=20)

EPOCHS = 500

tensorboard = keras.callbacks.TensorBoard(log_dir="logs/{}".format(time()))

# Treina modelo
history = model.fit(train_data, train_labels, epochs=EPOCHS,
                    validation_split=0.2, verbose=0,
                    callbacks=[early_stop,tensorboard])

# Dados do para teste do modelo
test_data = np.array([
    [2020, 11, 9]
])
test_data = (test_data - mean) / std

# Prevê a quantidade de vendas no mês baseado nos dados test_data
test_predictions = model.predict(test_data).flatten()

print(test_predictions)